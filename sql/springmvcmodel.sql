/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : springmvcmodel

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2017-06-18 02:22:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagename` varchar(255) DEFAULT NULL,
  `sendtime` varchar(255) DEFAULT NULL,
  `fromuser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of image
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('5', 'user5', 'pass5');
INSERT INTO `user` VALUES ('6', 'user6', 'pass6');
INSERT INTO `user` VALUES ('7', 'user7', 'pass7');
INSERT INTO `user` VALUES ('8', 'user8', 'pass8');
INSERT INTO `user` VALUES ('9', 'user9', 'pass9');
INSERT INTO `user` VALUES ('10', 'user10', 'pass10');
INSERT INTO `user` VALUES ('11', 'user11', 'pass11');
INSERT INTO `user` VALUES ('12', 'user12', 'pass12');
INSERT INTO `user` VALUES ('13', 'user13', 'pass13');
INSERT INTO `user` VALUES ('14', 'user14', 'pass14');
INSERT INTO `user` VALUES ('15', 'user15', 'pass14');
INSERT INTO `user` VALUES ('16', 'user16', 'pass16');
INSERT INTO `user` VALUES ('17', 'user17', 'pass17');
INSERT INTO `user` VALUES ('18', 'user18', 'pass18');
INSERT INTO `user` VALUES ('19', 'user19', 'pass19');
INSERT INTO `user` VALUES ('20', 'user20', 'pass20');

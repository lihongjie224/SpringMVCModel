package com.springmvcmodel.dao;

import com.springmvcmodel.domain.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper{
    List<User> selectByParam(@Param("user")User user);

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    int selectUserListLength(@Param("user")User user);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}
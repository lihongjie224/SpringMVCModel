package com.springmvcmodel.dao;

import com.springmvcmodel.domain.Image;
import com.springmvcmodel.domain.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ImageMapper {
    List<Image> selectByParam(@Param("image")Image image);

    int deleteByPrimaryKey(Integer id);

    int insert(Image record);

    int insertSelective(Image record);

    Image selectByPrimaryKey(Integer id);

    int selectImageListLength(@Param("image")Image image);

    int updateByPrimaryKeySelective(Image record);

    int updateByPrimaryKey(Image record);
}
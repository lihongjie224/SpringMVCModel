package com.springmvcmodel.service;

import com.springmvcmodel.domain.Image;

import java.util.List;

/**
 * Created by Administrator on 2017/6/18.
 */
public interface IImageService {
    public Image getImageById(int id);

    public List<Image> getImageList(Image image);

    public boolean deleteImageById(int id);

    public Integer getImageListLength(Image image);

    public boolean insertImage(Image image);

    public boolean updateImage(Image image);
}

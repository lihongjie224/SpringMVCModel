package com.springmvcmodel.service;

import com.springmvcmodel.domain.User;

import java.util.List;

/**
 * Created by Administrator on 2017/3/29.
 */
public interface IUserService {
    public User getUserById(int userId);

    public List<User> getUserList(User user);

    public Integer getUserListLength(User user);

    public boolean deleteUserById(int id);

    public boolean insertUser(User user);

    public boolean updateUser(User user);
}

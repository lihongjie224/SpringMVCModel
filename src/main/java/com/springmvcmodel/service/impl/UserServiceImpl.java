package com.springmvcmodel.service.impl;

import com.springmvcmodel.dao.UserMapper;
import com.springmvcmodel.domain.User;
import com.springmvcmodel.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017/3/29.
 */
@Service("userService")
public class UserServiceImpl implements IUserService {

    @Resource
    private UserMapper userMapper;
    public User getUserById(int userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    public List<User> getUserList(User user) {
        return userMapper.selectByParam(user);
    }

    public Integer getUserListLength(User user) {
        return userMapper.selectUserListLength(user);
    }

    public boolean deleteUserById(int id) {
        return userMapper.deleteByPrimaryKey(id)>0?true:false;
    }

    public boolean insertUser(User user) {
        return userMapper.insert(user)>0?true:false;
    }

    public boolean updateUser(User user) {
        return userMapper.updateByPrimaryKeySelective(user)>0?true:false;
    }

}

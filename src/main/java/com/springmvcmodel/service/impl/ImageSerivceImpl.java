package com.springmvcmodel.service.impl;

import com.springmvcmodel.dao.ImageMapper;
import com.springmvcmodel.domain.Image;
import com.springmvcmodel.service.IImageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2017/6/18.
 */
@Service("imageService")
public class ImageSerivceImpl implements IImageService{
    @Resource
    private ImageMapper imageMapper;

    public Image getImageById(int id) {
        return imageMapper.selectByPrimaryKey(id);
    }

    public List<Image> getImageList(Image image) {
        return imageMapper.selectByParam(image);
    }

    public boolean deleteImageById(int id) {
        return imageMapper.deleteByPrimaryKey(id)>0?true:false;
    }

    public Integer getImageListLength(Image image) {
        return imageMapper.selectImageListLength(image);
    }

    public boolean insertImage(Image image) {
        return imageMapper.insert(image)>0?true:false;
    }

    public boolean updateImage(Image image) {
        return imageMapper.updateByPrimaryKeySelective(image)>0?true:false;
    }
}

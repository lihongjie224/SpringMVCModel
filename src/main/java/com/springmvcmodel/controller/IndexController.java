package com.springmvcmodel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;

/**
 * Created by Administrator on 2017/3/29.
 */
@Controller
@RequestMapping("/")
public class IndexController {
    @RequestMapping("/index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView();
        SpringTemplateEngine engine = new SpringTemplateEngine();
        view.setViewName("index");
        return view;
    }
}

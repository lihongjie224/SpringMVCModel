package com.springmvcmodel.controller;

import com.springmvcmodel.domain.User;
import com.springmvcmodel.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/29.
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;

    @RequestMapping("/index")
    public ModelAndView userList(User user) {
        ModelAndView view = new ModelAndView();
        view.setViewName("user_list");
        view.addObject("data",userService.getUserList(user));
        view.addObject("length",userService.getUserListLength(user));
        return view;
    }

    @RequestMapping("/getUserById")
    @ResponseBody
    public User getUserById(int id) {
        return userService.getUserById(id);
    }

    @RequestMapping("/deleteUserById")
    @ResponseBody
    public boolean deleteUserById(int id) {
        return userService.deleteUserById(id);
    }

    @RequestMapping("/toInsertUserPage")
    public ModelAndView toInsertUserPage() {
        ModelAndView view = new ModelAndView();
        view.setViewName("user_insert");
        return view;
    }

    @RequestMapping("/insertUser")
    @ResponseBody
    public boolean insertUser(User user) {
        return userService.insertUser(user);
    }

    @RequestMapping("/toUpdateUserPage")
    public ModelAndView toUpdateUserPage(int id) {
        ModelAndView view = new ModelAndView();
        view.addObject("user",userService.getUserById(id));
        view.setViewName("user_update");
        return view;
    }

    @RequestMapping("/updateUser")
    @ResponseBody
    public boolean updateUser(User user) {
        return userService.updateUser(user);
    }
}

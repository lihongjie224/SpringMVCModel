package com.springmvcmodel.controller;

import com.springmvcmodel.domain.Image;
import com.springmvcmodel.service.IImageService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Administrator on 2017/6/18.
 */
@Controller
@RequestMapping("/image")
public class ImageController {
    @Autowired
    private IImageService imageService;

    @RequestMapping("/index")
    public ModelAndView index(Image image) {
        ModelAndView view = new ModelAndView();
        view.setViewName("image_list");
        view.addObject("data",imageService.getImageList(image));
        view.addObject("length",imageService.getImageListLength(image));
        return view;
    }

    @RequestMapping("/uploadImage")
    @ResponseBody
    public String  uploadImage(@RequestParam("file") MultipartFile file,String fromUser) {
        try {
            byte[] data = file.getBytes();
            File outputDir = new File("D:/upload/");
            if(!outputDir.exists()) {
                outputDir.mkdir();
            }
            String fileName = UUID.randomUUID().toString();
            outputDir = new File(outputDir,fileName+".jpg");
            BufferedInputStream bis = new BufferedInputStream(new ByteArrayInputStream(data));
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outputDir));
            int len;
            byte[] buff = new byte[2048];
            while(-1!=(len=bis.read(buff,0,buff.length))) {
                bos.write(buff,0,len);
            }
            bis.close();
            bos.close();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Image image = new Image();
            image.setImagename(fileName);
            image.setFromuser(fromUser);
            image.setSendtime(dateFormat.format(new Date()));
            imageService.insertImage(image);
            return fileName;
        } catch (Exception e) {
            return "file not upload!";
        }
    }

    @RequestMapping("/showImage")
    @ResponseBody
    public void showImage(HttpServletResponse response,String name) {
        try {
            String fileName = "D:/upload/"+name+".jpg";
            File file = new File(fileName);
            FileInputStream inputStream = new FileInputStream(file);
            byte[] data = new byte[(int)file.length()];
            int length = inputStream.read(data);
            inputStream.close();
            response.setContentType("image/jpeg");
            OutputStream stream = response.getOutputStream();
            stream.write(data);
            stream.flush();
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
